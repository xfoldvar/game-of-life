<?php

/**
 * Created by PhpStorm.
 * User: Apex
 * Date: 06.12.2016
 * Time: 18:38
 */
class Cell
{
    /**
     * @var number
     * X coordinate
     */
    private $x;
    /**
     * @var number
     * Y coordinate
     */
    private $y;
    /**
     * @var bool
     * Holds state of cell
     */
    private $dead;
    /**
     * @var array
     * Holds all cell neighbours
     */
    private $neighbours;
    /**
     * @var
     * Holds next state of cell
     */
    private $will_die;

    public function __construct($x, $y, $dead = true)
    {
        $this->x = $x;
        $this->y = $y;
        $this->dead = $dead;
        $this->neighbours = array();
    }

    /**
     * @return string
     * Returns symbol representing cell state
     */
    public function toString(){
        //$result = $this->dead ? "&#10686 [$this->x $this->y] " : "&#10687 [$this->x $this->y] "; // dead : alive
        $result = $this->dead ? "&#10686" : "&#10687"; // dead : alive
        return $result;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @return null
     */
    public function getNeighbours()
    {
        return $this->neighbours;
    }

    public function addNeighbour($cell){
        array_push($this->neighbours,$cell);
    }

    /**
     * @return boolean
     */
    public function isDead()
    {
        return $this->dead;
    }

    /**
     * @return mixed
     */
    public function getWillDie()
    {
        return $this->will_die;
    }

    /**
     * @param mixed $will_die
     */
    public function setWillDie($will_die)
    {
        $this->will_die = $will_die;
    }

    /**
     * @param boolean $dead
     */
    public function setDead($dead)
    {
        $this->dead = $dead;
    }
}