<?php

require_once ('World.php');
/**
 * Created by PhpStorm.
 * User: Apex
 * Date: 06.12.2016
 * Time: 19:28
 */
final class Engine
{
    /**
     * @var World
     * Holds instance of world
     */
    private $world;
    /**
     * @var
     * Singleton
     */
    private static $engine;
    /**
     * @var number
     * Holds size of matrix
     */
    private $width, $height;

    private function __construct(){
        $this->world = new World();
    }

     /**
     * @return mixed
      * Singleton
     */
    public function getInstance()
    {
        if(self::$engine != null) return self::$engine;
        else return self::$engine = new Engine();
    }

    /**
     * @param $width
     * @param $height
     * Creates matrix and add cells
     */
    public function createMatrix($width,$height){
        $this->width = $width;
        $this->height = $height;
        for($i = 1; $i<=$height;$i++){
            for($j = 1; $j<=$width;$j++){
                $will_be_alive = $this->world->will_cell_live();
                $this->world->add_cell($j,$i,$will_be_alive);
            }
        }
    }

    /**
     * Prints out the world
     */
    public function world_to_string()
    {
        foreach ($this->world->world_to_string($this->width) as $row) echo $row;
    }

    /**
     * @param $clock
     * Ticks the clock of world
     */
    public function raise_clock($clock){
        for($i = 1; $i<=$clock; $i++){
            echo "<h6> $i. tick of clock </h6>";
            $this->world->raise_clock();
            $this->world_to_string();
        }
    }
}