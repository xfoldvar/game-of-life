<?php

require_once ('Cell.php');

/**
 * Created by PhpStorm.
 * User: Apex
 * Date: 06.12.2016
 * Time: 18:47
 */
class World
{
    /**
     * @var number
     * Holds current clock of world
     */
    private $clock;
    /**
     * @var array
     * Holds all cells
     */
    private $cells;
    /**
     * @var array
     * Holds all possible directions
     */
    private $directions =
        array
        (   //    [x],[y]
            //array(0,0),//CURRENT CELL
            array(1,-1), //UPPER RIGHT CORNER CELL
            array(0,-1),  //ABOVE CELL
            array(-1,-1),  //UPPER LEFT CORNER CELL
            array(-1,0), //LEFT CELL
            array(1,0),  //RIGHT CELL
            array(-1,1),//LOWER LEFT CORNER CELL
            array(0,1), //BELOW CELL
            array(1,1)  //LOWER RIGHT CORNER CELL
        );

    public function __construct()
    {
        $this->clock = 0;
        $this->cells = array();
    }

    /**
     * @param $x
     * @param $y
     * @return mixed|null
     * Checks if cell with coordinates x y exists
     */
    private function occupied($x,$y){
        $occupied = null;
        if(isset($this->cells["$x|$y"])) $occupied = $this->cells["$x|$y"];
        return $occupied;
    }

    /**
     * @param $x
     * @param $y
     * @param bool $dead
     * @return Cell|null
     * Creates cell if position is not occupied
     */
    public function add_cell($x,$y, $dead = true){
        if($this->occupied($x,$y)==null){
            $cell = new Cell($x,$y, $dead);
            $this->cells["$x|$y"] = $cell;
            return $cell;
        }
        return null;
    }

    /**
     * @param $cell
     * @return mixed
     * Returns array of cell neighbours
     */
    public function cell_neighbours($cell){
        if($cell->getNeighbours()==null){
            foreach ($this->directions as $direction){
                $neighbour = $this->occupied($cell->getX() + $direction[0], $cell->getY() + $direction[1]);
                if($neighbour!=null) $cell->addNeighbour($neighbour);
            }
        }
        return $cell->getNeighbours();
    }

    /**
     * @param $cell
     * @return int
     * Counts neighbour of cell which are alive
     */
    public function count_alive_neighbours($cell){
        $count = 0;
        $cells = $this->cell_neighbours($cell);
        foreach($cells as $neighbour) if(!$neighbour->isDead())$count++;
        return $count;
    }


    /**
     * Computes next step in world
     */
    public function raise_clock(){
        foreach ($this->cells as $cell) {
            $this->god($cell);
        }
        foreach ($this->cells as $cell){
            if($cell->getWillDie()==true)
                $cell->setDead(true);
            else
                $cell->setDead(false);
        }
        $this->clock++;
    }

    /**
     * @param $cell
     * Main game logic
     * S23/B3
     */
    private function god($cell){
        $alive = $this->count_alive_neighbours($cell);
        if($cell->isDead()==false){
            if($alive==2||$alive==3){
                $cell->setWillDie(false);
            }else{
                $cell->setWillDie(true);
            }
        } else {
            if($alive==3){
                $cell->setWillDie(false);
            }else{
                $cell->setWillDie(true);
            }
        }
    }

    /**
     * @return bool
     * Used for plant init cells
     */
    public function will_cell_live(){
        return rand(0,100) <= 30 ? true : false;
    }

    /**
     * @param $width
     * @return array
     * Returns array of cells to string
     */
    public function world_to_string($width){
        $rows = array();
        $row = "";
        $count = 0;
        foreach ($this->cells as $cell){
            $count++;
            $row .= $cell->toString() . " ";
            if($count == $width){
                $row .= "<br>";
                array_push($rows, $row);
                $count = 0;
                $row = "";
            }
        }
        return $rows;
    }


}