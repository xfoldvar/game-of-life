<?php
require_once ('Engine.php');
/**
 * Created by PhpStorm.
 * User: Apex
 * Date: 06.12.2016
 * Time: 21:03
 */
echo "<p> DEAD &#10686 | ALIVE &#10687 </p>";
//Get instance of Engine
$engine = Engine::getInstance();
//Creates matrix in engine instance
$engine->createMatrix(20,20);
echo "<h5> WORLD INIT </h5>";
//Prints out initialized world
$engine->world_to_string();
//Do 10 ticks
$engine->raise_clock(10);